﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Grupo
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public Grupo() { }
        public Grupo(int pId, string pNombre)
        {
            Id = pId;
            Nombre = pNombre;
        }
    }
}
