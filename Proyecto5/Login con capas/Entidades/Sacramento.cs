﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Sacramento
    {
        public int IdSacramento { get; set; }
        public DateTime FechaSacramento { get; set; }
        public string NombrePadrino { get; set; }
        public string ApellidoPadrino { get; set; }
        public string NombreMadrina { get; set; }
        public string ApellidoMadrina { get; set; }
        public int? LibroSacramento { get; set; }
        public int? FolioSacramento { get; set; }
        public string LugarPartida { get; set; }
        public DateTime FechaPartida { get; set; }
        public int TipoSacramento { get; set; }

        public Sacramento() { }
        public Sacramento(int pIdSacramento, DateTime pFechaSacramento, string pNombrePadrino, string pApellidoPadrino, string pNombreMadrina, string pApellidoMadrina, int? pLibroSacramento, int? pFolioSacramento, string pLugarPartida, DateTime pFechaPartida)
        {
            IdSacramento = pIdSacramento;
            FechaSacramento = pFechaSacramento;
            NombrePadrino = pNombrePadrino;
            ApellidoPadrino = pApellidoPadrino;
            NombreMadrina = pNombreMadrina;
            ApellidoMadrina = pApellidoMadrina;
            LibroSacramento = pLibroSacramento;
            FolioSacramento = pFolioSacramento;
            LugarPartida = pLugarPartida;
            FechaPartida = pFechaPartida;
        }
    }
}
