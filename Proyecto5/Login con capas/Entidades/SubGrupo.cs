﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class SubGrupo
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Grupo { get; set; }
        public SubGrupo() { }
        public SubGrupo(int pId, string pNombre, int pGrupo)
        {
            Id = pId;
            Nombre = pNombre;
            Grupo = pGrupo;
        }
    }
}
