﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Bitacora
    {
        public int Id { get; set; }
        public int Usuario { get; set; }
        public DateTime Fecha { get; set; }
        public string Descripcion { get; set; }
        public Bitacora() { }
        public Bitacora(int pId, int pUsuario, DateTime pFecha, string pDescripcion)
        {
            Id = pId;
            Usuario = pUsuario;
            Fecha = pFecha;
            Descripcion = pDescripcion;
        }
    }
}
