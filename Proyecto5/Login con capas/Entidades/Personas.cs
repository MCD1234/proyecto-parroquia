﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Personas
    {
        public int IdPersona { get; set; }
        public string NombresPersona { get; set; }
        public string ApellidosPersona { get; set; }
        public int DNIPersona { get; set; }
        public int TipoPersona { get; set; }
        public DateTime FechaNacimientoPersona { get; set; }
        public string TelefonoPersona { get; set; }
        public string LugarNacimientoPersona { get; set; }
        public string NombreMadre { get; set; }
        public string ApellidoMadre { get; set; }
        public string NombrePadre { get; set; }
        public string ApellidoPadre { get; set; }
        public Personas() { }
        public Personas(int pId, string pNombres, string pApellidos, int pDNI, int pTipo, DateTime pFechaNacimiento, string pTelefono, string plugarnacimiento, string pnombremadre, string papellidomadre, string pnombrepadre, string papellidopadre)
        {
            IdPersona = pId;
            NombresPersona = pNombres;
            ApellidosPersona = pApellidos;
            DNIPersona = pDNI;
            TipoPersona = pTipo;
            FechaNacimientoPersona = pFechaNacimiento;
            TelefonoPersona = pTelefono;
            LugarNacimientoPersona = plugarnacimiento;
            NombreMadre = pnombremadre;
            ApellidoMadre = papellidomadre;
            NombrePadre = pnombrepadre;
            ApellidoPadre = papellidopadre;
        }

    }
}
