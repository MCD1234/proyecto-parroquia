﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Usuarios //entidad de los usuarios que ingresan al sistema
    {
        public int Id { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public int Persona { get; set; }
        public bool Activo { get; set; }
        public DateTime Bloqueo { get; set; }

        public Usuarios() { }
        public Usuarios(int pId, string pUsuario, string pCotraseña, bool pActivo, int pPersona, DateTime pBloqueo)
        {
            Id = pId;
            Usuario = pUsuario;
            Password = pCotraseña;
            Activo = pActivo;
            Bloqueo = pBloqueo;
            Persona = pPersona;
        }
    }
}