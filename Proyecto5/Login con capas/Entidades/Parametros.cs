﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Parametros
    {
        public string NombreParametro { get; set; }
        public string ValorParametro { get; set; }
    }
}
