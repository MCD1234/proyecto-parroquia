﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Acceso_a_base_de_datos;
using Entidades;

namespace Regla_de_negocios
{
    public class UsuariosRG
    {
        Comandos comandos = new Comandos();
        public Boolean AltaUsuario(Usuarios u)
        {
            List<Parametros> ListaParametros = new List<Parametros>();
            Parametros parametro1 = new Parametros();
            parametro1.NombreParametro = "@v_usuario";
            parametro1.ValorParametro = u.Usuario;
            ListaParametros.Add(parametro1);
            Parametros parametro2 = new Parametros();
            parametro2.NombreParametro = "@v_contraseña";
            parametro2.ValorParametro = Encriptación.GetMD5(u.Password);
            ListaParametros.Add(parametro2);
            string Stored = "InsertUsuario";
            return comandos.EjecutarStore(Stored, ListaParametros);
        }
        public Boolean BajaUsuario(Usuarios u)
        {
            List<Parametros> ListaParametros = new List<Parametros>();
            Parametros nombre = new Parametros();
            nombre.NombreParametro = "@v_id";
            nombre.ValorParametro = u.Id.ToString();
            ListaParametros.Add(nombre);
            string Stored = "BajaUsuario";
            return comandos.EjecutarStore(Stored, ListaParametros);
        }
        public Boolean ModificarUsuario(Usuarios u)
        {
            List<Parametros> ListaParametros = new List<Parametros>();
            Parametros parametro1 = new Parametros();
            parametro1.NombreParametro = "@v_id";
            parametro1.ValorParametro = u.Id.ToString();
            ListaParametros.Add(parametro1);
            Parametros parametro2 = new Parametros();
            parametro2.NombreParametro = "@v_usuario";
            parametro2.ValorParametro = u.Usuario;
            ListaParametros.Add(parametro2);
            Parametros parametro3 = new Parametros();
            parametro3.NombreParametro = "@v_contraseña";
            parametro3.ValorParametro = Encriptación.GetMD5(u.Password);
            ListaParametros.Add(parametro3);
            string Stored = "ModificarUsuario";
            return comandos.EjecutarStore(Stored, ListaParametros);
        }
        public Boolean InsertarBloqueo(Usuarios u)
        {
            List<Parametros> ListaDeParametros = new List<Parametros>();
            Parametros parametro1 = new Parametros();
            parametro1.NombreParametro = "@p_fecha";
            parametro1.ValorParametro = u.Bloqueo.ToString();
            ListaDeParametros.Add(parametro1);
            Parametros parametro2 = new Parametros();
            parametro2.NombreParametro = "@p_id";
            parametro2.ValorParametro = u.Id.ToString();
            ListaDeParametros.Add(parametro2);
            string Stored = "InsertarBloqueo";
            return comandos.EjecutarStore(Stored, ListaDeParametros);
        }
        public List<Usuarios> SeleccionarUsuario(Usuarios u)
        {
            Usuarios Usuarios = new Usuarios();
            List<Usuarios> LstUsuarios = new List<Usuarios>();
            List<Parametros> listaparametros = new List<Parametros>();
            Parametros usuario = new Parametros();
            usuario.NombreParametro = "@v_usuario";
            usuario.ValorParametro = u.Usuario;
            listaparametros.Add(usuario);
            string Stored = "Logueo";
            SqlDataReader reader = comandos.EjecutarReader(Stored, listaparametros);
            while (reader.Read())
            {
                Parametros contraseña = new Parametros();
                contraseña.NombreParametro = "@v_contraseña";
                contraseña.ValorParametro = Encriptación.GetMD5(u.Password);
                listaparametros.Add(contraseña);
                Usuarios.Id = int.Parse(reader["IdUsuario"].ToString());
                Usuarios.Usuario = reader["Usuario"].ToString();
                Usuarios.Bloqueo = DateTime.Parse(reader["BloqueoUsuario"].ToString());
                Usuarios.Password = reader["PasswordUsuario"].ToString();
            }
            LstUsuarios.Add(Usuarios);
            return LstUsuarios;
        }
        public DataTable ListarUsuarios()
        {
            string NombreTabla = "Usuario";
            string Stored = "ListarUsuarios";
            return comandos.Dataset(Stored, NombreTabla);
        }
        public Boolean AltaPersona(Personas p)
        {
            List<Parametros> ListaParametros = new List<Parametros>();
            Parametros parametro1 = new Parametros();
            parametro1.NombreParametro = "@vnombre";
            parametro1.ValorParametro = p.NombresPersona;
            ListaParametros.Add(parametro1);
            Parametros parametro2 = new Parametros();
            parametro2.NombreParametro = "@vapellido";
            parametro2.ValorParametro = p.ApellidosPersona;
            ListaParametros.Add(parametro2);


            string Stored = "InsertarPersona";
            return comandos.EjecutarStore(Stored, ListaParametros);
            //vdni vtipo vfechanacimiento vtelefono vlugarnacimiento vnombremadre vapellidomadre vnombrepadre vapellidopadre
        }
    }
}