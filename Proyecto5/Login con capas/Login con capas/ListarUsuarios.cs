﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Regla_de_negocios;
using System.Windows.Forms;

namespace Login_con_capas
{
    public partial class ListarUsuarios : Form
    {
        public ListarUsuarios()
        {
            InitializeComponent();
        }

        private void Listar_Click(object sender, EventArgs e)
        {
            UsuariosRG rg = new UsuariosRG();
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = rg.ListarUsuarios();
        }

        private void ListarUsuarios_Load(object sender, EventArgs e)
        {

        }
    }
}
