﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using System.IO;
using Microsoft.Reporting.WinForms;

namespace Login_con_capas
{
    public partial class Reporte : Form
    {
        public Reporte()
        {
            InitializeComponent();
        }

        private void Reporte_Load(object sender, EventArgs e)
        {

            reportViewer1.LocalReport.ReportPath = Path.Combine(Application.StartupPath, "Report1.rdlc");

            //hago una lista porque solo acepta colecciones.
            List<Sacramento> lst = new List<Sacramento>();

            Sacramento sacramento = new Sacramento();
            sacramento.FechaSacramento = DateTime.Now;
            sacramento.NombreMadrina = "asdasd";
            sacramento.ApellidoMadrina = "dasdasd";
            sacramento.NombrePadrino = "sadsasd";
            sacramento.ApellidoPadrino = "sdasd";
            sacramento.LibroSacramento = 1;
            sacramento.FolioSacramento = 2;
            sacramento.LugarPartida = "Buenos Aires, ";
            sacramento.FechaPartida = DateTime.Now;

            lst.Add(sacramento);
            ReportDataSource dr = new ReportDataSource("Datos", lst);
            reportViewer1.LocalReport.DataSources.Add(dr);
            //bindingSource1.DataSource = sacramento;
             
            this.reportViewer1.RefreshReport();
        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {

        }
    }
}
