﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Regla_de_negocios;
using Acceso_a_base_de_datos;
using Entidades;

namespace Login_con_capas
{

    public partial class Logueo : Form
    {
        UsuariosRG usuarioRG = new UsuariosRG();
        int contador = 0;
        public Logueo()
        {
            InitializeComponent();
        }
        void Limpiar()
        {
            txtUsuario.Clear();
            txtContraseña.Clear();
        }
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            contador++;
            Usuarios u = new Usuarios();
            List<Usuarios> usuarios = new List<Usuarios>();
            u.Usuario = txtUsuario.Text;
            u.Password = txtContraseña.Text;
            usuarios = usuarioRG.SeleccionarUsuario(u);
            foreach (Usuarios usuario in usuarios)
            {
                if (txtUsuario.Text == usuario.Usuario)
                {
                    int id = usuario.Id;
                    if (txtUsuario.Text == usuario.Usuario && Encriptación.GetMD5(txtContraseña.Text) == usuario.Password)
                    {
                        if(usuario.Bloqueo < DateTime.Now)
                        {
                            MessageBox.Show("Usuario y contraseña correcta");
                         //   Contenedor contenedor = new Contenedor();
                           // contenedor.Show();
                            this.Hide();
                        }
                        else
                        {
                            MessageBox.Show("No puede ingresar, usuario bloqueado");
                            Application.Exit();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Contraseña incorrecta");
                        if (contador == 3)
                        {
                            MessageBox.Show("Limite alcanzado, usuario bloqueado por 10 minutos", "Aviso", MessageBoxButtons.OK,MessageBoxIcon.Error);
                            usuario.Bloqueo = DateTime.Now.AddMinutes(10);
                            usuarioRG.InsertarBloqueo(usuario);
                            Application.Exit();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Usuario incorrecto");
                    Limpiar();

                }
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}