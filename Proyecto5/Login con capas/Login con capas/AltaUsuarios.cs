﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Regla_de_negocios;
using Entidades;

namespace Login_con_capas
{
    public partial class AltaUsuarios : Form
    {
        public AltaUsuarios()
        {
            InitializeComponent();
        }
        void Limpiar()
        {
            txtConfContra.Clear();
            txtContraseña.Clear();
            txtUsuario.Clear();
            txtUsuario.Focus();
        }
        UsuariosRG usuarioRG = new UsuariosRG();
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (txtContraseña.Text == txtConfContra.Text)
            {
                Usuarios u = new Usuarios();
                u.Usuario = txtUsuario.Text;
                u.Password = txtContraseña.Text;
                u.Activo = true;
                if (usuarioRG.AltaUsuario(u) == true)
                {
                    MessageBox.Show("El usuario se guardo correctamente", "Aviso");
                    Limpiar();
                }
                else
                {
                    MessageBox.Show("No se pudo guardar el usuario", "Aviso");
                    Limpiar();
                }
            }
            else
            {
                MessageBox.Show("Las contraseña no coinciden", "Aviso");
                Limpiar();
            }
        }
    }
}