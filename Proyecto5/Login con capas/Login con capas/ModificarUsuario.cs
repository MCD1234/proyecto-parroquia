﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Regla_de_negocios;
using Entidades;

namespace Login_con_capas
{
    public partial class ModificarUsuario : Form
    {
        public ModificarUsuario()
        {
            InitializeComponent();
        }
        UsuariosRG usuarioRG = new UsuariosRG();
        Usuarios u = new Usuarios();
        void CargarDatos()
        {
            cmbUsuario.DataSource = usuarioRG.ListarUsuarios();
            cmbUsuario.DisplayMember = "Usuario";
            cmbUsuario.ValueMember = "IdUsuario";
            cmbUsuario.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbUsuario.SelectedIndex = -1;
        }
        void Borrar()
        {
            txtConfContra.Clear();
            txtContraseña.Clear();
            txtContraseña.Focus();
        }
        private void ModificarUsuario_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (cmbUsuario.SelectedIndex != -1)
            {
                u.Id = int.Parse(cmbUsuario.SelectedValue.ToString());
                u.Usuario = cmbUsuario.Text;
                txtUsuario.Text = cmbUsuario.Text;
            }
            else
            {
                MessageBox.Show("Seleccione un usuario");
            }
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (txtContraseña.Text == txtConfContra.Text)
            {
                u.Password = txtContraseña.Text;
                if (usuarioRG.ModificarUsuario(u) == true)
                {
                    MessageBox.Show("Se guardaron los cambios");
                    Borrar();
                }
                else
                {
                    MessageBox.Show("No se pudieron guardar los cambios");
                    Borrar();
                }
            }
            else
            {
                MessageBox.Show("Las contraseña no coinciden");
                Borrar();
            }
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Borrar();
        }
    }
}