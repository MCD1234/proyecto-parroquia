﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Regla_de_negocios;
using Entidades;

namespace Login_con_capas
{
    public partial class BajaUsuario : Form
    {
        public BajaUsuario()
        {
            InitializeComponent();
        }
        void CargarDatos()
        {
            cmbUsuario.DataSource = usuarioRG.ListarUsuarios();
            cmbUsuario.DisplayMember = "Usuario";
            cmbUsuario.ValueMember = "Id";
            cmbUsuario.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbUsuario.SelectedIndex = -1;
        }
        UsuariosRG usuarioRG = new UsuariosRG();
        private void BajaUsuario_Load(object sender, EventArgs e)
        {
            CargarDatos();
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if(cmbUsuario.SelectedIndex != -1 )
            {
                Usuarios u = new Usuarios();
                u.Id = int.Parse(cmbUsuario.SelectedValue.ToString());
                if (usuarioRG.BajaUsuario(u) == true)
                {
                    MessageBox.Show("El usuario fue dado de baja", "Aviso");
                    CargarDatos();
                }
                else
                {
                    MessageBox.Show("No se pudo dar de baja el usuario", "Aviso");
                    cmbUsuario.SelectedIndex = -1;
                }
            }
            else
            {
                MessageBox.Show("Seleccione un usuario");
            }
        }
    }
}
