﻿namespace Login_con_capas
{
    partial class AltaPersona
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.txtnombres = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtapellidos = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtdni = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txttelefono = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtlugarnac = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtnombrema = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtapellidoma = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtnombrepa = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtapellidopa = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txttipo = new System.Windows.Forms.ComboBox();
            this.txtfechanac = new System.Windows.Forms.DateTimePicker();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 18);
            this.label2.TabIndex = 12;
            this.label2.Text = "Nombres";
            // 
            // txtnombres
            // 
            this.txtnombres.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnombres.Location = new System.Drawing.Point(179, 19);
            this.txtnombres.Name = "txtnombres";
            this.txtnombres.PasswordChar = '*';
            this.txtnombres.Size = new System.Drawing.Size(151, 26);
            this.txtnombres.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 18);
            this.label3.TabIndex = 13;
            this.label3.Text = "Apellidos";
            // 
            // txtapellidos
            // 
            this.txtapellidos.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtapellidos.Location = new System.Drawing.Point(179, 53);
            this.txtapellidos.Name = "txtapellidos";
            this.txtapellidos.PasswordChar = '*';
            this.txtapellidos.Size = new System.Drawing.Size(151, 26);
            this.txtapellidos.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 18);
            this.label1.TabIndex = 16;
            this.label1.Text = "DNI";
            // 
            // txtdni
            // 
            this.txtdni.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdni.Location = new System.Drawing.Point(179, 89);
            this.txtdni.Name = "txtdni";
            this.txtdni.PasswordChar = '*';
            this.txtdni.Size = new System.Drawing.Size(151, 26);
            this.txtdni.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(154, 18);
            this.label4.TabIndex = 17;
            this.label4.Text = "Fecha de nacimiento";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 159);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 18);
            this.label5.TabIndex = 20;
            this.label5.Text = "Teléfono";
            // 
            // txttelefono
            // 
            this.txttelefono.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttelefono.Location = new System.Drawing.Point(179, 157);
            this.txttelefono.Name = "txttelefono";
            this.txttelefono.PasswordChar = '*';
            this.txttelefono.Size = new System.Drawing.Size(151, 26);
            this.txttelefono.TabIndex = 18;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(10, 193);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(150, 18);
            this.label6.TabIndex = 21;
            this.label6.Text = "Lugar de nacimiento";
            // 
            // txtlugarnac
            // 
            this.txtlugarnac.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlugarnac.Location = new System.Drawing.Point(179, 191);
            this.txtlugarnac.Name = "txtlugarnac";
            this.txtlugarnac.PasswordChar = '*';
            this.txtlugarnac.Size = new System.Drawing.Size(151, 26);
            this.txtlugarnac.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 229);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 18);
            this.label7.TabIndex = 24;
            this.label7.Text = "Nombre madre";
            // 
            // txtnombrema
            // 
            this.txtnombrema.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnombrema.Location = new System.Drawing.Point(179, 227);
            this.txtnombrema.Name = "txtnombrema";
            this.txtnombrema.PasswordChar = '*';
            this.txtnombrema.Size = new System.Drawing.Size(151, 26);
            this.txtnombrema.TabIndex = 22;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(10, 263);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(114, 18);
            this.label8.TabIndex = 25;
            this.label8.Text = "Apellido madre";
            // 
            // txtapellidoma
            // 
            this.txtapellidoma.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtapellidoma.Location = new System.Drawing.Point(179, 261);
            this.txtapellidoma.Name = "txtapellidoma";
            this.txtapellidoma.PasswordChar = '*';
            this.txtapellidoma.Size = new System.Drawing.Size(151, 26);
            this.txtapellidoma.TabIndex = 23;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 300);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(109, 18);
            this.label9.TabIndex = 28;
            this.label9.Text = "Nombre padre";
            // 
            // txtnombrepa
            // 
            this.txtnombrepa.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnombrepa.Location = new System.Drawing.Point(179, 297);
            this.txtnombrepa.Name = "txtnombrepa";
            this.txtnombrepa.PasswordChar = '*';
            this.txtnombrepa.Size = new System.Drawing.Size(151, 26);
            this.txtnombrepa.TabIndex = 26;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(10, 334);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(110, 18);
            this.label10.TabIndex = 29;
            this.label10.Text = "Apellido padre";
            // 
            // txtapellidopa
            // 
            this.txtapellidopa.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtapellidopa.Location = new System.Drawing.Point(179, 332);
            this.txtapellidopa.Name = "txtapellidopa";
            this.txtapellidopa.PasswordChar = '*';
            this.txtapellidopa.Size = new System.Drawing.Size(151, 26);
            this.txtapellidopa.TabIndex = 27;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(10, 368);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 18);
            this.label11.TabIndex = 32;
            this.label11.Text = "Tipo";
            // 
            // txttipo
            // 
            this.txttipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.txttipo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttipo.FormattingEnabled = true;
            this.txttipo.Items.AddRange(new object[] {
            "Catequista",
            "Catecúmeno",
            "Personal",
            "Sacerdote"});
            this.txttipo.Location = new System.Drawing.Point(179, 362);
            this.txttipo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txttipo.Name = "txttipo";
            this.txttipo.Size = new System.Drawing.Size(151, 26);
            this.txttipo.TabIndex = 33;
            // 
            // txtfechanac
            // 
            this.txtfechanac.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfechanac.Location = new System.Drawing.Point(179, 120);
            this.txtfechanac.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtfechanac.Name = "txtfechanac";
            this.txtfechanac.Size = new System.Drawing.Size(151, 26);
            this.txtfechanac.TabIndex = 34;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAceptar.Location = new System.Drawing.Point(254, 392);
            this.btnAceptar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(82, 35);
            this.btnAceptar.TabIndex = 35;
            this.btnAceptar.Text = "&Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // AltaPersona
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 436);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.txtfechanac);
            this.Controls.Add(this.txttipo);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtnombrepa);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtapellidopa);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtnombrema);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtapellidoma);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txttelefono);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtlugarnac);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtdni);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtnombres);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtapellidos);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "AltaPersona";
            this.Text = "AltaPersona";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtnombres;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtapellidos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtdni;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txttelefono;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtlugarnac;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtnombrema;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtapellidoma;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtnombrepa;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtapellidopa;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox txttipo;
        private System.Windows.Forms.DateTimePicker txtfechanac;
        private System.Windows.Forms.Button btnAceptar;
    }
}