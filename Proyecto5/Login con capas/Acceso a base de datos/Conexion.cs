﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;

namespace Acceso_a_base_de_datos
{
    public class Conexion //clase utilizada para obtener la conexión establecida en appconfig
    {
        public SqlConnection ObtenerDireccion()
        {
            SqlConnection cone = new SqlConnection(ConfigurationManager.AppSettings["Conexion"].ToString());
            return cone;
        }
    }
}