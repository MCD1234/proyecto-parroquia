﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Entidades;

namespace Acceso_a_base_de_datos
{
    public class Comandos
    {
        Conexion cnn = new Conexion();
        public Boolean EjecutarStore(string Stored, List<Parametros> parametros)
        {
            bool retornar = true;
            SqlConnection conexion = cnn.ObtenerDireccion();
            conexion.Open();
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexion;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = Stored;
            try
            {
                foreach (Parametros p in parametros)
                {
                    comando.Parameters.AddWithValue(p.NombreParametro, p.ValorParametro);
                }
                comando.ExecuteNonQuery();
                retornar = true;
            }
            catch (Exception)
            {
                retornar = false;
            }
            finally
            {
                conexion.Close();
            }
            return retornar;
        }
        public SqlDataReader EjecutarReader(string Stored, List<Parametros> parametros)
        {
            SqlConnection conexion = cnn.ObtenerDireccion();
            conexion.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexion;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = Stored;
            foreach (Parametros p in parametros)
            {
                cmd.Parameters.AddWithValue(p.NombreParametro, p.ValorParametro);
            }
            SqlDataReader Reader = cmd.ExecuteReader();
            return Reader;
        }
        public DataTable Dataset(string Stored, string NombreTabla)
        {
            DataSet dataset = new DataSet();
            SqlConnection conexion = cnn.ObtenerDireccion();
            conexion.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexion;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = Stored;
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dataset, NombreTabla);
            return dataset.Tables[NombreTabla];
        }
    }
}