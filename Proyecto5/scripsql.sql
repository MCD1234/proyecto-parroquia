USE [master]
GO
/****** Object:  Database [Proyecto]    Script Date: 21/09/2017 23:59:09 ******/
CREATE DATABASE [Proyecto]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Login nuevo', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Proyecto.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Login nuevo_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Proyecto_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Proyecto] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Proyecto].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Proyecto] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Proyecto] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Proyecto] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Proyecto] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Proyecto] SET ARITHABORT OFF 
GO
ALTER DATABASE [Proyecto] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Proyecto] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Proyecto] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Proyecto] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Proyecto] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Proyecto] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Proyecto] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Proyecto] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Proyecto] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Proyecto] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Proyecto] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Proyecto] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Proyecto] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Proyecto] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Proyecto] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Proyecto] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Proyecto] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Proyecto] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Proyecto] SET  MULTI_USER 
GO
ALTER DATABASE [Proyecto] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Proyecto] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Proyecto] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Proyecto] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Proyecto] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Proyecto]
GO
/****** Object:  Table [dbo].[Bitacora]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Bitacora](
	[IdBitacora] [int] IDENTITY(1,1) NOT NULL,
	[UsuarioBitacora] [int] NULL,
	[FechaBitacora] [datetime] NOT NULL,
	[DescripcionBitacora] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Bitácora] PRIMARY KEY CLUSTERED 
(
	[IdBitacora] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Evento]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Evento](
	[IdEvento] [int] IDENTITY(1,1) NOT NULL,
	[DescripcionEvento] [varchar](50) NOT NULL,
	[SacerdoteEncargadoEvento] [int] NOT NULL,
	[SacramentoEvento] [int] NULL,
 CONSTRAINT [PK_Evento] PRIMARY KEY CLUSTERED 
(
	[IdEvento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Grupo]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Grupo](
	[IdGrupo] [int] IDENTITY(1,1) NOT NULL,
	[NombreGrupo] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Grupo] PRIMARY KEY CLUSTERED 
(
	[IdGrupo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Persona]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Persona](
	[IdPersona] [int] IDENTITY(1,1) NOT NULL,
	[NombresPersona] [varchar](50) NOT NULL,
	[ApellidosPersona] [varchar](100) NOT NULL,
	[DNIPersona] [int] NOT NULL,
	[TipoPersona] [int] NOT NULL,
	[FechaNacimientoPersona] [datetime] NOT NULL,
	[TelefonoPersona] [varchar](50) NULL,
	[LugarNacimientoPersona] [varchar](50) NULL,
	[NombreMadre] [varchar](50) NULL,
	[ApellidoMadre] [varchar](50) NULL,
	[NombrePadre] [varchar](50) NULL,
	[ApellidoPadre] [varchar](50) NULL,
 CONSTRAINT [PK_Persona] PRIMARY KEY CLUSTERED 
(
	[IdPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonaEvento]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonaEvento](
	[IdPersonaEvento] [int] NOT NULL,
	[PersonaPersonaEvento] [int] NOT NULL,
	[EventoPersonaEvento] [int] NOT NULL,
 CONSTRAINT [PK_PersonaEvento] PRIMARY KEY CLUSTERED 
(
	[IdPersonaEvento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersonaSacramento]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonaSacramento](
	[IdPersonaSacramento] [int] IDENTITY(1,1) NOT NULL,
	[PersonaPersonaSacramento] [int] NOT NULL,
	[SacramentoPersonaSacramento] [int] NOT NULL,
 CONSTRAINT [PK_PersonaSacramento] PRIMARY KEY CLUSTERED 
(
	[IdPersonaSacramento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersonaSubGrupo]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonaSubGrupo](
	[IdPersonaSubgrupo] [int] IDENTITY(1,1) NOT NULL,
	[PersonaPersonaSubGrupo] [int] NOT NULL,
	[SubgrupoPersonaSubGrupo] [int] NOT NULL,
 CONSTRAINT [PK_PersonaSubGrupo] PRIMARY KEY CLUSTERED 
(
	[IdPersonaSubgrupo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersonaTurno]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonaTurno](
	[IdPersonaTurno] [int] IDENTITY(1,1) NOT NULL,
	[PersonaPersonaTurno] [int] NOT NULL,
	[TurnoPersonaTurno] [int] NOT NULL,
 CONSTRAINT [PK_PersonaTurno] PRIMARY KEY CLUSTERED 
(
	[IdPersonaTurno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Rol]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rol](
	[IdRol] [int] IDENTITY(1,1) NOT NULL,
	[NombreRol] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Rol] PRIMARY KEY CLUSTERED 
(
	[IdRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sacramento]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sacramento](
	[IdSacramento] [int] NOT NULL,
	[FechaSacramento] [date] NOT NULL,
	[NombrePadrino] [varchar](50) NULL,
	[ApellidoPadrino] [varchar](50) NULL,
	[NombreMadrina] [varchar](50) NULL,
	[ApellidoMadrina] [varchar](50) NULL,
	[LibroSacramento] [int] NULL,
	[FolioSacramento] [int] NULL,
	[LugarPartida] [varchar](50) NOT NULL,
	[FechaPartida] [date] NOT NULL,
	[TipoSacramento] [int] NOT NULL,
 CONSTRAINT [PK_Sacramento] PRIMARY KEY CLUSTERED 
(
	[IdSacramento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SubGrupo]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubGrupo](
	[IdSubGrupo] [int] IDENTITY(1,1) NOT NULL,
	[NombreSubGrupo] [varchar](50) NOT NULL,
	[GrupoSubGrupo] [int] NOT NULL,
 CONSTRAINT [PK_SubGrupo] PRIMARY KEY CLUSTERED 
(
	[IdSubGrupo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TipoPersona]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoPersona](
	[IdTipoPersona] [int] IDENTITY(1,1) NOT NULL,
	[NombreTipoPersona] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TipoPersona] PRIMARY KEY CLUSTERED 
(
	[IdTipoPersona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TipoSacramento]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoSacramento](
	[IdTipoSacramento] [int] IDENTITY(1,1) NOT NULL,
	[NombreTipoSacramento] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Sacramento_1] PRIMARY KEY CLUSTERED 
(
	[IdTipoSacramento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Turno]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Turno](
	[IdTurno] [int] IDENTITY(1,1) NOT NULL,
	[EventoTurno] [int] NOT NULL,
	[PersonaTurno] [int] NOT NULL,
 CONSTRAINT [PK_Turno] PRIMARY KEY CLUSTERED 
(
	[IdTurno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[IdUsuario] [int] IDENTITY(1,1) NOT NULL,
	[Usuario] [varchar](50) NOT NULL,
	[PasswordUsuario] [varchar](50) NOT NULL,
	[PersonaUsuario] [int] NOT NULL,
	[RolUsuario] [int] NOT NULL,
	[ActivoUsuario] [bit] NOT NULL,
	[BloqueoUsuario] [datetime] NOT NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__Login__E3237CF796646A39] UNIQUE NONCLUSTERED 
(
	[Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Bitacora]  WITH CHECK ADD  CONSTRAINT [FK_Bitacora_Usuario] FOREIGN KEY([UsuarioBitacora])
REFERENCES [dbo].[Usuario] ([IdUsuario])
GO
ALTER TABLE [dbo].[Bitacora] CHECK CONSTRAINT [FK_Bitacora_Usuario]
GO
ALTER TABLE [dbo].[Evento]  WITH CHECK ADD  CONSTRAINT [FK_Evento_Persona1] FOREIGN KEY([SacerdoteEncargadoEvento])
REFERENCES [dbo].[Persona] ([IdPersona])
GO
ALTER TABLE [dbo].[Evento] CHECK CONSTRAINT [FK_Evento_Persona1]
GO
ALTER TABLE [dbo].[Evento]  WITH CHECK ADD  CONSTRAINT [FK_Evento_Sacramento] FOREIGN KEY([SacramentoEvento])
REFERENCES [dbo].[TipoSacramento] ([IdTipoSacramento])
GO
ALTER TABLE [dbo].[Evento] CHECK CONSTRAINT [FK_Evento_Sacramento]
GO
ALTER TABLE [dbo].[Persona]  WITH CHECK ADD  CONSTRAINT [FK_Persona_TipoPersona] FOREIGN KEY([TipoPersona])
REFERENCES [dbo].[TipoPersona] ([IdTipoPersona])
GO
ALTER TABLE [dbo].[Persona] CHECK CONSTRAINT [FK_Persona_TipoPersona]
GO
ALTER TABLE [dbo].[PersonaEvento]  WITH CHECK ADD  CONSTRAINT [FK_PersonaEvento_Evento] FOREIGN KEY([EventoPersonaEvento])
REFERENCES [dbo].[Evento] ([IdEvento])
GO
ALTER TABLE [dbo].[PersonaEvento] CHECK CONSTRAINT [FK_PersonaEvento_Evento]
GO
ALTER TABLE [dbo].[PersonaEvento]  WITH CHECK ADD  CONSTRAINT [FK_PersonaEvento_Persona] FOREIGN KEY([PersonaPersonaEvento])
REFERENCES [dbo].[Persona] ([IdPersona])
GO
ALTER TABLE [dbo].[PersonaEvento] CHECK CONSTRAINT [FK_PersonaEvento_Persona]
GO
ALTER TABLE [dbo].[PersonaSacramento]  WITH CHECK ADD  CONSTRAINT [FK_PersonaSacramento_Persona] FOREIGN KEY([PersonaPersonaSacramento])
REFERENCES [dbo].[Persona] ([IdPersona])
GO
ALTER TABLE [dbo].[PersonaSacramento] CHECK CONSTRAINT [FK_PersonaSacramento_Persona]
GO
ALTER TABLE [dbo].[PersonaSacramento]  WITH CHECK ADD  CONSTRAINT [FK_PersonaSacramento_Sacramento1] FOREIGN KEY([SacramentoPersonaSacramento])
REFERENCES [dbo].[Sacramento] ([IdSacramento])
GO
ALTER TABLE [dbo].[PersonaSacramento] CHECK CONSTRAINT [FK_PersonaSacramento_Sacramento1]
GO
ALTER TABLE [dbo].[PersonaSubGrupo]  WITH CHECK ADD  CONSTRAINT [FK_PersonaSubGrupo_Persona] FOREIGN KEY([PersonaPersonaSubGrupo])
REFERENCES [dbo].[Persona] ([IdPersona])
GO
ALTER TABLE [dbo].[PersonaSubGrupo] CHECK CONSTRAINT [FK_PersonaSubGrupo_Persona]
GO
ALTER TABLE [dbo].[PersonaSubGrupo]  WITH CHECK ADD  CONSTRAINT [FK_PersonaSubGrupo_SubGrupo] FOREIGN KEY([SubgrupoPersonaSubGrupo])
REFERENCES [dbo].[SubGrupo] ([IdSubGrupo])
GO
ALTER TABLE [dbo].[PersonaSubGrupo] CHECK CONSTRAINT [FK_PersonaSubGrupo_SubGrupo]
GO
ALTER TABLE [dbo].[PersonaTurno]  WITH CHECK ADD  CONSTRAINT [FK_PersonaTurno_Persona] FOREIGN KEY([PersonaPersonaTurno])
REFERENCES [dbo].[Persona] ([IdPersona])
GO
ALTER TABLE [dbo].[PersonaTurno] CHECK CONSTRAINT [FK_PersonaTurno_Persona]
GO
ALTER TABLE [dbo].[PersonaTurno]  WITH CHECK ADD  CONSTRAINT [FK_PersonaTurno_Turno] FOREIGN KEY([TurnoPersonaTurno])
REFERENCES [dbo].[Turno] ([IdTurno])
GO
ALTER TABLE [dbo].[PersonaTurno] CHECK CONSTRAINT [FK_PersonaTurno_Turno]
GO
ALTER TABLE [dbo].[Sacramento]  WITH CHECK ADD  CONSTRAINT [FK_Sacramento_TipoSacramento] FOREIGN KEY([TipoSacramento])
REFERENCES [dbo].[TipoSacramento] ([IdTipoSacramento])
GO
ALTER TABLE [dbo].[Sacramento] CHECK CONSTRAINT [FK_Sacramento_TipoSacramento]
GO
ALTER TABLE [dbo].[SubGrupo]  WITH CHECK ADD  CONSTRAINT [FK_SubGrupo_Grupo] FOREIGN KEY([GrupoSubGrupo])
REFERENCES [dbo].[Grupo] ([IdGrupo])
GO
ALTER TABLE [dbo].[SubGrupo] CHECK CONSTRAINT [FK_SubGrupo_Grupo]
GO
ALTER TABLE [dbo].[Turno]  WITH CHECK ADD  CONSTRAINT [FK_Turno_Evento] FOREIGN KEY([EventoTurno])
REFERENCES [dbo].[Evento] ([IdEvento])
GO
ALTER TABLE [dbo].[Turno] CHECK CONSTRAINT [FK_Turno_Evento]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Persona] FOREIGN KEY([PersonaUsuario])
REFERENCES [dbo].[Persona] ([IdPersona])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Persona]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Rol] FOREIGN KEY([RolUsuario])
REFERENCES [dbo].[Rol] ([IdRol])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Rol]
GO
/****** Object:  StoredProcedure [dbo].[BajaUsuario]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[BajaUsuario]
@v_id varchar(50) as
update Usuario set ActivoUsuario=0 where IdUsuario=CONVERT(int,@v_id)
GO
/****** Object:  StoredProcedure [dbo].[InsertarPersona]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[InsertarPersona]
@vnombre varchar(100), @vapellido varchar(100), @vdni int, @vtipo int, @vfechanacimiento datetime, @vtelefono varchar(50), @vlugarnacimiento varchar(50), @vnombramadre varchar(50), @vapellidomadre varchar(50), @vnombrepadre varchar(50), @vapellidopadre varchar(50)
as insert into Persona(NombresPersona, ApellidosPersona, DNIPersona, TipoPersona, FechaNacimientoPersona, TelefonoPersona, LugarNacimientoPersona, NombreMadre, ApellidoMadre,NombrePadre,ApellidoPadre)
 values
(@vnombre, @vapellido, @vdni, @vtipo, @vfechanacimiento, @vtelefono, @vlugarnacimiento,@vnombramadre, @vapellidomadre, @vnombrepadre, @vapellidopadre)
GO
/****** Object:  StoredProcedure [dbo].[InsertUsuario]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[InsertUsuario]
@v_usuario varchar(50), @v_contraseña varchar(50), @v_persona int, @v_rol int
as insert into Usuario(Usuario, PasswordUsuario, ActivoUsuario, BloqueoUsuario, PersonaUsuario, RolUsuario)
values(@v_usuario, @v_contraseña, 1, GETDATE(), @v_persona, @v_rol)
GO
/****** Object:  StoredProcedure [dbo].[ListarUsuarios]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ListarUsuarios]
as select * from Usuario where ActivoUsuario=1
GO
/****** Object:  StoredProcedure [dbo].[Logueo]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Logueo]
@v_usuario varchar(50), @v_contraseña varchar(50)=null
as select * from Usuario where Usuario=@v_usuario and [PasswordUsuario]=isnull(@v_contraseña,[PasswordUsuario]) and ActivoUsuario=1
GO
/****** Object:  StoredProcedure [dbo].[ModificarUsuario]    Script Date: 21/09/2017 23:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ModificarUsuario]
@v_id varchar(50), @v_usuario varchar(50), @v_contraseña varchar(50)
as update Usuario set Usuario=@v_usuario, Password=@v_contraseña
where Id=CONVERT(int,@v_id)
GO
USE [master]
GO
ALTER DATABASE [Proyecto] SET  READ_WRITE 
GO
